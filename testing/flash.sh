#!/bin/bash

# Flash a thinkpad light. This must run as root

devicefile="/proc/acpi/ibm/kbdlight"
ledid=-1
values=""
delay=0.3
device=""
verbose=""

function usage {
    echo "Usage sudo $0 kbd|power"
}

if [[ 'root' != $(whoami) ]]; then
    echo "Run me as root"
    usage
    exit 1
fi

if [[ -z $1 ]]; then
    usage
    exit 1
else
    device="$1"
fi

if [[ $device == 'kbd' ]]; then
    devicefile="/proc/acpi/ibm/kbdlight"
    values="0 1 2 1 0"
elif [[ $device == 'power' ]]; then
    devicefile="/proc/acpi/ibm/led"
    ledid=0
    values="on off on off on"
else
    usage
    exit 1
fi

# blink
for value in $values ; do

    # Add ledid if necessary
    if [[ -1 -ne $ledid ]]; then
        value="$ledid $value"
    fi

    [[ $verbose ]] && echo "Running echo $value > $devicefile"
    echo "$value" > $devicefile
    sleep $delay
done

exit 0
