Pass on notifications to thinklight, hass or similar.

Run flash_on_notifications.py on any ubuntu laptop with keyboard light should flash keyboard lights on notifications.




-------------------------------------------------

scratch notes:
https://wiki.archlinux.org/index.php/keyboard_backlight

R/w for any user should be possible with:

sudo modprobe -r ec_sys
sudo modprobe ec_sys write_support=1

Test with:
echo "0 blink" | sudo tee /proc/acpi/ibm/led 1> /dev/null




#!/bin/bash
#chmod 666 /sys/class/leds/tpacpi\:\:thinklight/brightness

[ "$1" == "0" ] && echo 0 > /sys/class/leds/tpacpi\:\:thinklight/brightness
[ "$1" == "1" ] && echo 255 > /sys/class/leds/tpacpi\:\:thinklight/brightness

https://www.reddit.com/r/thinkpad/comments/7n8eyu/thinkpad_led_control_under_gnulinux/

test notifications:
notify-send --urgency=low --expire-time=2000 --app-name=spotify --icon=disk --category=friendly Header Body
