#!/usr/bin/env python3
# Requires python3-gi

from gi.repository import GLib
import dbus
from dbus.mainloop.glib import DBusGMainLoop
import time

verbose = False
delay = 0.2

def print_notification(bus, message):
    keys = ["app_name", "replaces_id", "app_icon", "summary",
          "body", "actions", "hints", "expire_timeout"]
    args = message.get_args_list()
    if len(args) == 8:
        notification = dict([(keys[i], args[i]) for i in range(8)])
        print (notification["app_name"], notification["summary"], notification["body"])
        flash_kbd()
        if verbose:
            print (notification)

def flash_kbd():
    bus = dbus.SystemBus()
    kbd_backlight_proxy = bus.get_object('org.freedesktop.UPower', '/org/freedesktop/UPower/KbdBacklight')
    kbd_backlight = dbus.Interface(kbd_backlight_proxy, 'org.freedesktop.UPower.KbdBacklight')

    old = kbd_backlight.GetBrightness()
    minimum = 0
    maximum = kbd_backlight.GetMaxBrightness()
    if verbose:
        print("Max", maximum)

    for i in range(minimum, maximum):
        if verbose:
            print (i)
        kbd_backlight.SetBrightness(i)
        time.sleep(delay)

    time.sleep(delay)
    for i in range(maximum, minimum-1, -1):
        if verbose:
            print (i)
        kbd_backlight.SetBrightness(i)
        time.sleep(delay)

    #Reset brightness to old value
    time.sleep(delay)
    kbd_backlight.SetBrightness(old)

loop = DBusGMainLoop(set_as_default=True)
session_bus = dbus.SessionBus()
session_bus.add_match_string("type='method_call',interface='org.freedesktop.Notifications',member='Notify',eavesdrop=true")
session_bus.add_message_filter(print_notification)

GLib.MainLoop().run()
